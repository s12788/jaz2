package webapp.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@WebFilter("/loginForm")
public class LoginSessionFilter implements Filter {


	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest httRequest = (HttpServletRequest) request;
		HttpSession session = httRequest.getSession();
		
		if (session.getAttribute("logged")!=null){
			response.getWriter()
			.println("Powtórne wypelnienie formularza zostało zablokowane!");
			return;
		}
		
		chain.doFilter(request, response);
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {

		
	}
	
	@Override
	public void destroy() {
		
	}

}
