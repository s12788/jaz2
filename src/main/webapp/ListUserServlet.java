package webapp;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import repository.DummyUserRepository;
import domain.User;

/**
 * Servlet implementation class AddApplicantServlet
 */
@WebServlet("/list")
public class ListUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		
		StringBuilder stringBuilder = new StringBuilder();
		
		for (User c: DummyUserRepository.db){
			
			stringBuilder.append("<br>"+c.getUsername()+"<br>");
		}
		
		
		PrintWriter out = response.getWriter();	
		out.println(stringBuilder);
		out.close();

	}
	

}
