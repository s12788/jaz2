package repository;

import java.util.ArrayList;
import java.util.List;

import domain.User;
import domain.Role;

public class DummyUserRepository {
	
	
	static public User loggedUser;
	
	static public List<User> db 
	= new ArrayList <User>();


	static public User checkUserByUsername(String username) {
		
		for (User c: db){
			
			if (c.getUsername().equals(username)){
				return c;
			}
		}
		
		return null;
	}
	
	static public User verifyLoginUser(String username, String password) {
		
		for (User c: db){
			
			if (c.getUsername().equals(username) &&
					c.getPassword().equals(password)){
				return c;
			}
		}
		return null;
		
	}


	static public void add(User user) {
		db.add(user);
	}


	static public void setPremiumUser(User user) {
		
		user.setRole(Role.Premium);
	}

}
